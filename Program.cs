﻿using System;

namespace ex_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input a number and press <Enter> so that we can get started");//begining text
            
            var num1 = int.Parse(Console.ReadLine());

            // The times table from 1 to 12
            Console.WriteLine($"{num1} multiplied by 1 is {num1 * 1}");

            Console.WriteLine($"{num1} multiplied by 2 is {num1 * 2}");

            Console.WriteLine($"{num1} multiplied by 3 is {num1 * 3}");

            Console.WriteLine($"{num1} multiplied by 4 is {num1 * 4}");

            Console.WriteLine($"{num1} multiplied by 5 is {num1 * 5}");

            Console.WriteLine($"{num1} multiplied by 6 is {num1 * 6}");

            Console.WriteLine($"{num1} multiplied by 7 is {num1 * 7}");

            Console.WriteLine($"{num1} multiplied by 8 is {num1 * 8}");

            Console.WriteLine($"{num1} multiplied by 9 is {num1 * 9}");

            Console.WriteLine($"{num1} multiplied by 10 is {num1 * 10}");

            Console.WriteLine($"{num1} multiplied by 11 is {num1 * 11}");

            Console.WriteLine($"{num1} multiplied by 12 is {num1 * 12}");

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();            


        }
    }
}
